"""""
Задача Эйлера 6
Сумма квадратов первых десяти натуральных чисел равна
1**2 + 2**2 + ... + 10**2 = 385
Квадрат суммы первых десяти натуральных чисел равен
(1 + 2 + ... + 10)**2 = 55**2 = 3025
Следовательно, 
разность между суммой квадратов и квадратом суммы первых десяти натуральных чисел составляет 3025 − 385 = 2640.
Найдите разность между суммой квадратов и квадратом суммы первых ста натуральных чисел.
"""""
import time
start = time.time()
sum_kvadr = []
kvadr_sum = []
for i in range(1, 101):
    square = i ** 2
    sum_kvadr.append(square)
print(sum(sum_kvadr))  # Сумма квадратов
for i in range(1, 101):
    square1 = i
    kvadr_sum.append(square1)
end = time.time()
print(sum(kvadr_sum) ** 2)
print('Разность между суммой квадратов и квадратом суммы первых ста натуральных чисел это',
      (sum(kvadr_sum) ** 2) - (sum(sum_kvadr)))
print('Время выполнения программы', end - start)
