"""""
Задача Эйлера 9
Тройка Пифагора - три натуральных числа a < b < c, для которых выполняется равенство
a**2 + b**2 = c**2
Например, 3**2 + 4**2 = 9 + 16 = 25 = 5**2.
Существует только одна тройка Пифагора, для которой a + b + c = 1000.
Найдите произведение abc.
"""""
import time
start = time.time()
first = 0
second = 0
for a in range(1, 1000):
    for b in range(1, 1000):
        if a + b + (a ** 2 + b ** 2) ** 0.5 == 1000:
            first = a
            second = b
            print(first, second)
c = 1000 - first - second
end = time.time()
print(c)
print('Произведение abc', first * second * c)
print('Время выполнения программы', end - start)
